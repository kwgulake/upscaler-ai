# Image upscaler

Image upscaler using machine learning.

Provided are pretrained models using the dataset found at

http://www.eecs.berkeley.edu/Research/Projects/CS/vision/grouping/BSR/BSR_bsds500.tgz

## Download

As zip, if you clone you will get some old files about 5gb which are not in use anymore 

## Usage

If you want to reuse the pretrained model, a simple script as such is sufficient:

```py
import torch
from PIL import Image

model = torch.load('1lag.pth')
model = model.eval()

image = Image.open('image.jpg')
image = model.scale_image(image)
image.save('output.jpg', "JPEG")
```

## Train for yourself

The `train_scale_self` method of Convolutional-class takes an iterator as such:
```py
model = ConvolutionalNetworkModel()
iterator = iter(torchvision.datasets.ImageFolder('./images'))
model.train_scale_self(iterator)
```

Note that in order for the ImageFolder to work you would have to have a file structure as: `images/imagefolder/*.jpg`
