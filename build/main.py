import torch 
import torch.nn as nn 
import torchvision
from sys import argv
from PIL import Image
import PIL 
from tqdm import trange 
from torch.utils.data import DataLoader
from torchvision import transforms
import numpy as np
import faulthandler
import matplotlib.pyplot as plt 
from ConvolutionalNetworkModel import ConvolutionalNeuralNetworkModel
#from testmodell import ConvolutionalNeuralNetworkModel
import torch.optim as optim 
import os 

use_gpu = lambda x=True: torch.set_default_tensor_type(torch.cuda.FloatTensor 
                                             if torch.cuda.is_available() and x 
                                             else torch.FloatTensor)

## https://discuss.pytorch.org/t/global-gpu-flag/17195

use_gpu()

def show_image(image):
    if torch.is_tensor(image):
        transforms.ToPILImage(mode='RGB')(image.cpu()).show()
    else: image.show()

def plot_image(image):
    plt.imshow(image)
    plt.show()


def night_train():

    for n in range(5):
        
        model = ConvolutionalNeuralNetworkModel(conv_layers=n+1).cuda()

        trainfolder = '/home/hansk/Documents/upscaler-ai/data/WallpaperData/BSR/BSDS500/data/images/train/'

        iterator = iter(torchvision.datasets.ImageFolder(trainfolder))

        model.train_scale_self(iterator, n=50000)

        img =  torchvision.datasets.ImageFolder('/home/hansk/Documents/upscaler-ai/data/WallpaperData/BSR/BSDS500/data/images/test/')

        img_it = iter(img)

        t = "{}lag".format(n+1)

        try:
            os.mkdir('test/{}'.format(t))
        except Exception as _:
            t = n**2
            os.mkdir('test/{}'.format(t))

        num = 0
        print("Saving images")
        for bilde in img_it:
            model.scale_image(bilde[0]).save('models/{}/test_images/{}.jpg'.format(t, n), "JPEG")
            num+=1
        print("Images Saved")

        torch.save(model, "{}lag.pth".format(n+1))


def two_train():

    layers = [4, 5]

    for n in layers:
        model = ConvolutionalNeuralNetworkModel(conv_layers=n)

        trainfolder = '/home/hansk/Documents/upscaler-ai/data/WallpaperData/BSR/BSDS500/data/images/train/'
        iterator = iter(torchvision.datasets.ImageFolder(trainfolder))
        model.train_scale_self(iterator, n=10000)

        img =  torchvision.datasets.ImageFolder('/home/hansk/Documents/upscaler-ai/data/WallpaperData/BSR/BSDS500/data/images/test/')
        img_it = iter(img)
        t = "{}lag".format(n)
        
        try:
            os.mkdir('models/{}'.format(t))
            os.mkdir('models/{}/test_images'.format(t))
        except Exception as _:
            print("All good")

        num = 0
        print("Saving images")
        for bilde in img_it:
            model.scale_image(bilde[0]).save('models/{}/test_images/{}.jpg'.format(t, num), "JPEG")
            num+=1
        print("Images Saved")

        torch.save(model, "{}lag.pth".format(n))
    
if __name__ == "__main__":

    torch.cuda.empty_cache()
   
    n_m = input("Create new model?(y/n)\n")

    if n_m == "y": 
        n_layer = input("number of layers (1-5)")
        model = ConvolutionalNeuralNetworkModel(conv_layers=int(n_layer)).cuda()

        trainfolder = '/home/hansk/Documents/upscaler-ai/data/WallpaperData/BSR/BSDS500/data/images/train/'
        iterator = iter(torchvision.datasets.ImageFolder(trainfolder))
        model.train_scale_self(iterator, n=1000)
    else: 
        model_name = input("Model directory:\n")
        pot_models = ['models/'+model_name+'/'+s for s in os.listdir('models/'+model_name)]
        model = torch.load(pot_models[-1])
        model = model.eval() 

    # Memory error exploration 

    """ import gc
    for obj in gc.get_objects():
        try:
            if torch.is_tensor(obj) or (hasattr(obj, 'data') and torch.is_tensor(obj.data)):
                print(type(obj), obj.size())
        except:
            pass """

    ## test model on image after training/loading:
    
    img =  torchvision.datasets.ImageFolder('/home/hansk/Documents/upscaler-ai/data/WallpaperData/BSR/BSDS500/data/images/test/')
    
    if os.path.exists('models') is False:
        os.mkdir('models')

    try:
        t = model_name
    except NameError as e:
        from datetime import datetime
        t = datetime.now().microsecond
        
        try:
            os.mkdir('models/{}'.format(t))
            os.mkdir('models/{}/test_images'.format(t))
        except _ as sssss:
            t = datetime.now().microsecond
            os.mkdir('models/{}'.format(t))
            os.mkdir('models/{}/test_images'.format(t))
    
    img_it = iter(img)
    n = 0

    print("Saving images")
    for bilde in img_it:
        model.scale_image(bilde[0]).save('models/{}/test_images/{}.jpg'.format(t, n), "JPEG")
        n+=1
    print("Images saved under ./models/{}/test_images".format(t))

    if n_m == "y":
        torch.save(model, "models/{}/default.pth".format(t))
        print("Saved model as models/{}/default.pth".format(t))