import torch 
import torch.nn as nn 
from torchvision import transforms
import torchvision
from tqdm import trange
from PIL import Image
import PIL 
from torch.utils.data import DataLoader
import os 
import numpy as np  

class ConvolutionalNeuralNetworkModel(nn.Module):
    def __init__(self, conv_layers=4):
        super(ConvolutionalNeuralNetworkModel, self).__init__()

        conv_layer = [nn.Conv2d(2**(n+3), 2**(n+4), (3,3), padding=(1,1)) for n in range(conv_layers)]
        conv_layer[0] = nn.Conv2d(3, 16, (3,3), padding=(1,1))

        if conv_layers == 1: 
            self.logits = nn.Sequential(
                    conv_layer[0],
                    nn.LeakyReLU(0.01),
                    nn.ConvTranspose2d(16, 3, (4, 4), (2, 2), (1, 1), (0, 0), bias=False),
                    nn.Sigmoid()
            )
        elif conv_layers == 2:
            self.logits = nn.Sequential(
                    conv_layer[0],
                    nn.LeakyReLU(0.01),
                    conv_layer[1],
                    nn.LeakyReLU(0.1),
                    nn.ConvTranspose2d(32, 3, (4, 4), (2, 2), (1, 1), (0, 0), bias=False),
                    nn.Sigmoid()
            )
        elif conv_layers == 3:
            self.logits = nn.Sequential(
                    conv_layer[0],
                    nn.LeakyReLU(0.01),
                    conv_layer[1],
                    nn.LeakyReLU(0.1),
                    conv_layer[2],
                    nn.LeakyReLU(0.1),
                    nn.ConvTranspose2d(64, 3, (4, 4), (2, 2), (1, 1), (0, 0), bias=False),
                    nn.Sigmoid()
            )
        elif conv_layers == 4: 
            self.logits = nn.Sequential(
                    conv_layer[0],
                    nn.LeakyReLU(0.01),
                    conv_layer[1],
                    nn.LeakyReLU(0.1),
                    conv_layer[2],
                    nn.LeakyReLU(0.1),
                    conv_layer[3],
                    nn.LeakyReLU(0.1),
                    nn.ConvTranspose2d(128, 3, (4, 4), (2, 2), (1, 1), (0, 0), bias=False),
                    nn.Sigmoid()
            )
        elif conv_layers == 5:
            self.logits = nn.Sequential(
                    conv_layer[0],
                    nn.LeakyReLU(0.01),
                    conv_layer[1],
                    nn.LeakyReLU(0.1),
                    conv_layer[2],
                    nn.LeakyReLU(0.1),
                    conv_layer[3],
                    nn.LeakyReLU(0.1),
                    conv_layer[4],
                    nn.LeakyReLU(0.1),
                    nn.ConvTranspose2d(256, 3, (4, 4), (2, 2), (1, 1), (0, 0), bias=False),
                    nn.Sigmoid()
            )

        else : raise SystemExit(0)

    def forward(self, x):
        return self.logits(x)

    ## https://heartbeat.fritz.ai/the-right-loss-function-pytorch-58d2c0d77404
    def loss(self, x, y):
        return nn.functional.mse_loss(self.logits(x), y)

    # Accuracy
    def accuracy(self, x, y):
        return 1 - torch.mean(self.logits(x) - y)


    def scale_image(self, image):
        image = transforms.ToTensor()(image)
        if torch.cuda.is_available() is True: image = image.cuda()
        
        image.requires_grad = True

        image = self.logits(torch.unsqueeze(image, 0))
        image = torch.squeeze(image, 0)

        if torch.cuda.is_available() is True: image = image.cpu()
        
        return transforms.ToPILImage()(image)

    def train_scale_self(self, train_iterator, n=10, lrn=0.0001):

        for y_train in train_iterator:
            y_train = y_train[0]

            s = y_train.size

            x_func = transforms.Compose([transforms.Resize((s[1]//2,s[0]//2), interpolation=1),transforms.ToTensor()]) 
            y_func = transforms.Compose([transforms.Resize(((s[1]//2)*2, (s[0]//2)*2), interpolation=1),transforms.ToTensor()])

            # scaling both x and y image
            # problem would arise if the y image is of odd w or h, as the logical size/2 would be half a pixel
            # which isnt possible - furthermore if we round down and the target size is odd, then we would get
            # matrix multiplication error 

            x_train = x_func(y_train)
            y_train = y_func(y_train)

            x_train = torch.unsqueeze(x_train, 0)
            y_train = torch.unsqueeze(y_train, 0)

            if torch.cuda.is_available() is True:
                y_train, x_train = y_train.cuda(), x_train.cuda()

            learning_rate = lrn
            optimizer = torch.optim.Adam(self.parameters(), lr=learning_rate)

            try:
                for _ in trange(n):
                    self.loss(x_train, y_train).backward()  # Compute loss gradients
                    optimizer.step()  # Perform optimization by adjusting W and b,
                    optimizer.zero_grad()  # Clear gradients for next step
            except Exception as err:
                print("{}\nContinuing".format(err))
                continue

